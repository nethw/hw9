import psutil

flag = True
for items in psutil.net_if_addrs().items():
    for item in items[1]:
        if items[0] == "en0" and flag:
            print(f"{items[0]} has address {item.address} with mask {item.netmask}")
            flag = False

if flag:
    print("Something went wrong")
