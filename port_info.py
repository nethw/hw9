import socket

address = input("Insert an IP to scan. ")
fromPort = int(input("scan ports from "))
toPort = int(input("to "))

print('Scanning...')
for port in range(fromPort, toPort + 1):
    client_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    client_socket.settimeout(0.5)
    if client_socket.connect_ex((address, port)) == 0:
        client_socket.close()
        print(f"Port {port} is open")